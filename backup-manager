#!/bin/bash
#
# List and fetch backups.

CONNSTR="9248@usw-s009.rsync.net"
RESTORE_LOC=/vicepa/hcoop-backups/restored

function get() {
    if test -n "$3" || test -z "$2"; then
        echo "Error: Invalid number of arguments for 'get' command."
        echo
        usage
    fi
    local date=$1
    local file=$2
    scp "$CONNSTR:$date/$file" "$RESTORE_LOC/$date-$file"
    local stat=$?
    echo
    if test $stat -eq 0; then
        echo "Get succeeded.  You may now run:"
        echo "  backup-manager restore $date '$file' VOLUME-NAME"
        echo "to restore the file into an AFS volume."
    else
        echo "Error: Get failed with status $stat."
        exit 1
    fi
}

function list() {
    if test -n "$2"; then
        echo "Error: Invalid number of arguments for 'list' command."
        echo
        usage
    fi
    local OUT=$(ssh $CONNSTR ls -l -h $@)
    local view=
    if test $(<<< "$OUT" wc -l) -ge $(tput lines); then
        # if output takes up all the lines on the screen, use a pager
        if test -n "$PAGER"; then
            view=$PAGER
        else
            view=less
        fi
    else
        view=cat
    fi
    <<< "$OUT" $view
}

function restore() {
    if test -n "$4" || test -z "$3"; then
        echo "Error: Invalid number of arguments for 'get' command."
        echo
        usage
    fi
    local date=$1
    local file=$2
    local volname=$3
    file="$RESTORE_LOC/$date-$file"
    if test ! -f "$file"; then
        echo "Error: File '$file' does not exist."
        exit 1
    fi
    vos examine "$volname" >/dev/null 2>&1
    local stat=$?
    if test $stat -eq 0; then
        echo "Error: Volume '$volname' already exists."
        exit 1
    fi
    < "$file" ccrypt -cdk /etc/backup-encryption-key \
        | gunzip | vos restore deleuze /vicepa "$volname"
    stat=$?
    echo
    if test $stat -eq 0; then
        echo "Restore succeeded.  You may now run:"
        echo "  fs mkm '/afs/hcoop.net/.old/$volname' '$volname'"
        echo "to mount the restored AFS volume."
    else
        echo "Error: Restore failed with status $stat."
        exit 1
    fi
}

function quota() {
    if test -n "$1"; then
        echo "Error: Invalid number of arguments for 'quota' command."
        echo
        usage
    fi
    ssh $CONNSTR quota
}

function usage() {
    echo "Usage: backup-manager [get|list|quota] [args...]"
    echo "Commands:"
    echo "  get DATE FILE    Fetch a file into $RESTORE_LOC"
    echo "  help             Display this message"
    echo "  list [DATE]      List dates available, or files with given date"
    echo "  quota            Display info about the quota"
    echo "  restore DATE FILE VOLUME-NAME"
    echo "                   Take a fetched file (referenced by DATE and" \
      "FILE) and turn"
    echo "                   it into a new AFS volume with the name" \
      "VOLUME-NAME"
    exit 1
}

# Dispatch
if test -z "$1"; then
    echo "Error: Incorrect number of arguments."
    echo
    usage
fi

cmd=$1
shift
case $cmd in
    get)     get $@     ;;
    help)    usage      ;;
    list)    list $@    ;;
    quota)   quota $@   ;;
    restore) restore $@ ;;
    *)
        echo "Error: Invalid command."
        echo
        usage
        ;;
esac

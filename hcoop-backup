#!/bin/bash -e

#
# it is dangerous to remove the "-e" above; please don't do that.
#

#
# run this script as root, on deleuze
#

PATH=$PATH:/bin:/usr/bin:/sbin:/usr/sbin
#COMPRESS_EXT=.bz2
#COMPRESS_PROG=bzip2
COMPRESS_EXT=.gz
COMPRESS_PROG=gzip
KEYFILE=/etc/backup-encryption-key
BACKUPTMP=/var/backups/hcoop-backup
CURDATE=$(date -u +%Y.%m.%d)

MOVE_OVER=$(dirname $0)/rsync.net-move-over

IFS=$'\n'

# Initialize storage area
RSYNCDIR=/vicepa/hcoop-backups/files
rm -fr $RSYNCDIR
mkdir -p $RSYNCDIR/$CURDATE
chmod og=rx,u=rwx $RSYNCDIR
chmod og= $RSYNCDIR/$CURDATE

# Initialize backup staging area
mkdir -p $BACKUPTMP
cd $BACKUPTMP

groups
echo "I am in: $(pwd)"
echo

echo "Building package lists..."
dpkg-query -W -f='${Package}\n' > packages
(cd /; find / /usr/ /usr/local/ /var/ -xdev)   | sort | uniq > allfiles
dpkg-query -W -f='${Package}\n'      | xargs dpkg -L | sort | uniq > debfiles
dpkg-query -W -f='${Conffiles}\n'           | grep / | cut -b2- | \
  sed 's_ .*__' | sort | uniq > conffiles

diff allfiles debfiles | grep '^<' | cut -b 3- | \
 grep -v ^/var/cache | \
 grep -v ^/var/tmp | \
 grep -v ^/var/lib/dpkg | \
 grep -v ^/var/backups | \
 grep -v ^/var/lib/changetrack | \
 grep -v ^/var/local/lib/spamd | \
 grep -v ^/var/run | \
 grep -v ^/var/lock | \
 grep -v ^/var/lib/ucf | \
 grep -v ^/vicepa | \
 grep -v ^/home | \
 grep -v ^/tmp | \
 grep -v '^/afs$' | \
 grep -v '^/$' | \
 grep -v '^/usr/$' | \
 grep -v ^/usr/src | \
 grep -v '^/usr/.*\.pyc' | \
 grep -v '^/usr/.*\.elc' | \
 grep -v '^/usr/bin/perldoc\.stub$' | \
 grep -v '^/usr/bin/.*\.notslocate$' | \
 grep -v '^/usr/lib/courier/.*\.rand$' | \
 grep -v '^/usr/lib/gconv/gconv-modules\.cache$' | \
 grep -v '^/usr/lib/ghc[^/*]/package\.conf$' | \
 grep -v '^/usr/lib/ghc[^/*]/package\.conf\.old$' | \
 grep -v '^/usr/lib/graphviz/config$' | \
 grep -v '^/usr/lib/locale/locale-archive$' | \
 grep -v '^/usr/share/emacs21/site-lisp/' | \
 grep -v '^/usr/share/emacs22/site-lisp/' | \
 grep -v '^/usr/share/info/dir$' | \
 grep -v '^/usr/share/info/dir\.old$' | \
 grep -v '^/usr/share/snmp/mibs/\.index$' | \
 grep -v '^/usr/share/vim/addons/doc/tags$' \
 > backupfiles

cat conffiles >> backupfiles

cat backupfiles | \
 grep -v ^/home | \
 grep -v ^/usr/local | \
 grep -v ^/var/spool | \
 grep -v ^/var/log | \
 grep -v ^/usr/lib/python2.4/ | \
 grep -v ^/var/lib/python-support | \
 grep -v ^/usr/share/jed/lib | \
 grep -v ^/usr/share/man | \
 grep -v ^/usr/share/perl5/IkiWiki/Plugin | \
 grep -v ^/media | \
 grep -v ^/vmlinuz | \
 grep -v ^/vmlinuz.old | \
 grep -v '^/sbin/[a-z\-]*\.modutils$' | \
 grep -v ^/opt | \
 grep -v ^/boot/ | \
 grep -v ^/dev/ | \
 grep -v ^/etc/ | \
 grep -v ^/root/ | \
 grep -v ^/var/ | \
 grep -v ^/lib/modules/ | \
 grep -v ^/var/domtool/ | \
 grep -v ^/var/lib/mysql/ | \
 grep -v ^/var/lib/postgres/ | \
 grep -v ^/var/lib/postgresql/ | \
 grep -v ^/usr/lib/ghc-6.6/ | \
 xargs -I{} -d\\n -- bash -c "test -L '{}' || echo '{}'" > complain

F=hcoop.backup.tar$COMPRESS_EXT.aescrypt
echo $COMPRESS_PROG $KEYFILE $MOVE_OVER $CURDATE $F
echo tar clpf - --ignore-failed-read --no-recursion -C / -T backupfiles | \
  $COMPRESS_PROG | \
  ccrypt -k $KEYFILE -e | \
  $MOVE_OVER $CURDATE $F


# Acquire lock before messing with spamd
COUNT=0
LOCK=/var/local/lib/spamd/.lock
while test -f $LOCK; do
    sleep 2m
    COUNT=$(expr $COUNT + 1)
    if test $COUNT -eq 10; then
        # Enough waiting.  Kill the process.
        P=$(cat $LOCK) || :
        test -n "$P" && kill $P || :
        rm -f $LOCK
        break
    fi
done
touch $LOCK

F=common.spamd.tar$COMPRESS_EXT.aescrypt
tar clpf - --ignore-failed-read -C / /var/local/lib/spamd | \
  $COMPRESS_PROG | \
  ccrypt -k $KEYFILE -e > $F
rm -f $LOCK
< $F $MOVE_OVER $CURDATE $F
rm -f $F

vos listvol fritz | \
  tail -n +2 | \
  head -n -3 | \
  cut -b1-32 | \
  grep -v "\.backup .*$" | \
  grep -v "\.readonly .*$" | \
  grep -v "\.d .*$" | \
  grep -v not-backed-up | \
  sed 's_^ .*__' | \
  sed 's_ .*$__' | \
  grep '[A-Za-z]' \
  > volumes

cat volumes | \
  xargs -I{} -d\\n -- \
  bash -e -c \
    "F={}.dump$COMPRESS_EXT.aescrypt ;
     vos dump -id {} -localauth -clone |
       $COMPRESS_PROG | ccrypt -k $KEYFILE -e |
       $MOVE_OVER $CURDATE \$F" || :

echo "Backing up databases ..."
F=databases.tar$COMPRESS_EXT.aescrypt
tar -C /var/backups/databases/ -cf - . | \
  $COMPRESS_PROG | \
  ccrypt -k $KEYFILE -e | \
  $MOVE_OVER $CURDATE $F

# Update file permissions so that rsync.net can access the backups
chmod -R go=,u-w $RSYNCDIR/$CURDATE
chmod u+w $RSYNCDIR/$CURDATE
chown -R rsync $RSYNCDIR/$CURDATE

# Complain to admins if there are unknown files
grep '[a-z/]' complain && \
  mail -a 'From: The Backup Program <backups@deleuze.hcoop.net>' \
       -s "automated message: annoying files found on deleuze (please do something about them)" admins@hcoop.net \
       < complain \
  || :

echo "Done."
